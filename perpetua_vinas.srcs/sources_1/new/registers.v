`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 09/17/2018 09:54:02 AM
// Design Name: 
// Module Name: registers
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module registers #(
    parameter reg_length=32,
    parameter reg_quantity=32,
    parameter bytes_number = $clog2(reg_quantity)
    )(
    input                                                       clk,
    
    input                                                       reset,
    input                                                       enable,
    
    input                                                       wr,
    input   [bytes_number-1:0]                                  w_address1,
    input   [reg_length-1:0]                                    w_data1,
        
    input   [bytes_number-1:0]                                  rt_address,
    input   [bytes_number-1:0]                                  rs_address,
    
    input                                                       debug,
    input   [bytes_number-1:0]                                  debug_address,
    
    output  reg[reg_length-1:0]                                 debug_data,
    
    output  reg[reg_length-1:0]                                 reg_rt,
    output  reg [reg_length-1:0]                                reg_rs
    );
    
    reg [reg_length-1:0]  registers [reg_quantity-1:0];
    
    always @(posedge clk)
    begin
        if (reset)
        begin
            reg_rt = 0;
            reg_rs = 0;
        end
        else if(enable)
        begin
            reg_rt <= registers[rt_address];
            reg_rs <= registers[rs_address];
       end
       if(debug)
            debug_data <= registers[debug_address];
        else
            debug_data <= 0;
    end

    always @(negedge clk)
    begin
        //if (!reset && enable) 
        //begin
            if (wr) 
            begin
                registers[w_address1] <= w_data1;                
            end
        //end
    end
    
endmodule
