`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 09/18/2018 03:43:23 PM
// Design Name: 
// Module Name: forwarding_unit
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ForwardingUnit #(
    parameter reg_length=32,
    parameter reg_quantity=32,
    parameter bytes_number = $clog2(reg_quantity)
    )(
        //From ID
        input           [bytes_number-1:0]                          rs,
        input           [bytes_number-1:0]                          rt,
        input           [reg_length-1:0]                            inst_id,
        
        //From EX
        input           [bytes_number-1:0]                          reg_dst_ex_mem,
        input                                                       reg_write_ex_mem,
        
        //From MEM
        input           [bytes_number-1:0]                          reg_dst_mem,
        input                                                       reg_write_mem,
        
        //To EX
        output  reg     [1:0]                                       mux1_alu_selector,
        output  reg     [1:0]                                       mux2_alu_selector
    );
    
    always @(*)
    begin
        case(inst_id[31:29])
            3'b000: 
            begin //R-type
            //First Mux.
                if(reg_write_ex_mem && reg_dst_ex_mem == rt)
                    mux1_alu_selector <= 2'b10;
                else if(reg_write_mem && reg_dst_mem == rt)
                    mux1_alu_selector <= 2'b11;
                else
                    mux1_alu_selector <= 2'b00;
            //Second mux.
                if(reg_write_ex_mem && reg_dst_ex_mem == rs)//With rd from ex
                    mux2_alu_selector <= 2'b10;
                else if(reg_write_mem && reg_dst_mem == rs)//With rd from mem
                    mux2_alu_selector <= 2'b11;
                else if(inst_id[5:2] == 4'b0000)//With Shampt
                    mux2_alu_selector <= 2'b00;
                else//With register
                    mux2_alu_selector <= 2'b01;
            end
            3'b001://Inmediates
            begin
                mux1_alu_selector <= 2'b01;
                if(reg_write_ex_mem && reg_dst_ex_mem == rs)//With rd from ex
                    mux2_alu_selector <= 2'b10;
                else if(reg_write_mem && reg_dst_mem == rs)//With rd from mem
                    mux2_alu_selector <= 2'b11;
                else//With register
                    mux2_alu_selector <= 2'b01;
            end
            3'b100,//Loads
            3'b101://Stores
            begin
                mux1_alu_selector <= 2'b01;
//                if(reg_write_ex_mem && reg_dst_ex_mem == rs)//With rd from ex
//                    mux2_alu_selector <= 2'b10;
//                else if(reg_write_mem && reg_dst_mem == rs)//With rd from mem
//                    mux2_alu_selector <= 2'b11;
//                else//With register
                    mux2_alu_selector <= 2'b01;
            end
            default:
            begin
                mux1_alu_selector <= 2'b11;
                mux2_alu_selector <= 2'b11;
            end
        endcase
    end
endmodule
