`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 09/14/2018 03:25:45 PM
// Design Name: 
// Module Name: InstructionDecoder
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module InstructionDecoder #(
    parameter   reg_length=32,
    parameter reg_quantity=32,
    parameter bytes_number = $clog2(reg_quantity)
    )(
    input                                                   clk,
    
    input                                                   reset,
    input                                                   enable,
    
    input                           [reg_length-1:0]        inst,
    input                           [reg_length-1:0]        address_pc,
    
    input                                                   reg_write,
    input                           [bytes_number-1:0]      reg_dst,
    input                           [reg_length-1:0]        reg_data,
    
    input                                                   halt_mem,
    
    input                                                   debug,
    input                           [bytes_number-1:0]      debug_address,
    output                          [reg_length-1:0]        debug_data,
    
    output                          [reg_length-1:0]        reg_rt,
    output                          [reg_length-1:0]        reg_rs,
    output                          [bytes_number-1:0]      rt,
    output                          [bytes_number-1:0]      rs,
    
    output                          [reg_length-1:0]        out_sign,
    
    output                          [bytes_number-1:0]      reg_dst_out,
    
    output                          [bytes_number-1:0]      out_shamt,
    
    output                          [3:0]                   alu_selector,
    
    output                          [reg_length-1:0]        inst_out,
    
    output                          [reg_length-1:0]        address_branch,
    output                                                  inst_selector,
    
    output                                                  mem_rd,
    output                                                  mem_wr,
    output                                                  reg_write_out,
    
    output                                                  halt_inst,
    output                                                  reg_dst_selector,
    
    output  reg                                             halt_wr
    );
    
    reg                         [reg_length-1:0]            r_address_target;
                        
    wire                                                    w_mux_reg_dst;

    wire                        [reg_length-1:0]            w_address_target;    
    wire                        [reg_length-1:0]            w_address_jump;
    wire                        [reg_length-1:0]            w_address;
        
    wire                                                    w_mux_inst;
    
    wire                                                    zero;
    
    wire                        [bytes_number-1:0]          w_rd;
    
    wire                                                    mux_branch;
    
    registers #(
        .reg_length(reg_length),
        .reg_quantity(32)
    ) regs(
        .clk(clk),
        .reset(reset),
        .enable(enable),
        .wr(reg_write),
        .w_address1(reg_dst),
        .w_data1(reg_data),
        .rt_address(inst[20:16]),
        .rs_address(inst[25:21]),
        .reg_rt(reg_rt),
        .reg_rs(reg_rs),
        
        .debug(debug),
        .debug_address(debug_address),
        .debug_data(debug_data)
    );
    
    decoder #(
        .reg_length(reg_length),
        .reg_quantity(reg_quantity),
        .bytes_number(bytes_number)
    ) DE (
        //Inputs
        .clk(clk),
        .enable(enable),
        .reset(reset),
        
        .inst(inst),
        .address_pc(address_pc),
        .zero(zero),
        
        //Outputs
            .alu_selector(alu_selector),
            .mux_reg_dst(w_mux_reg_dst),
            .mux_inst(inst_selector),
            .mem_rd(mem_rd),
            .mem_wr(mem_wr),
            .reg_write(reg_write_out),
            .mux_branch(mux_branch),
            .halt_inst(halt_inst),
            .inst_out(inst_out),
            .reg_dst_selector(reg_dst_selector)
    );
    
    spliter #(
        .reg_length(reg_length),
        .reg_quantity(reg_quantity),
        .bytes_number(bytes_number)
    ) spliter (
        .clk(clk),
        .reset(reset),
        .enable(enable),
        .inst(inst),
        .out_rt(rt),
        .out_rd(w_rd),
        .out_rs(rs),
        .out_shamt(out_shamt),
        .out_sign(out_sign),
        .address_jump(w_address_jump)
    );
    
    mux #(
        .reg_length(reg_length)   
    ) mux_pc(
        .in1(r_address_target),
        .in2(w_address_jump),
        .selector(mux_branch),
        .out(address_branch)
    );
    
    mux #(
        .reg_length(5)   
    ) mux_1(
        .selector(w_mux_reg_dst),
        .in1(w_rd),
        .in2(rt),
        .out(reg_dst_out)
    ); 
    
    alu #(
        .reg_length(reg_length)
    ) target(
        .a(address_pc),
        .b($signed(inst[15:0])),
        .op(3),
        .result(w_address_target)
    );
    
    alu #(
        .reg_length(reg_length)
    ) checker(
        .a(reg_rt),
        .b(reg_rs),
        .op(9),
        .zero(zero)
    );
    
    always @(posedge clk)//, posedge reset) 
        begin
            if (reset) 
            begin
                r_address_target <= 0;
                halt_wr <= 0;
            end
            else if(enable)
            begin
                r_address_target <= w_address_target;
                halt_wr <= halt_mem;
            end
        end
endmodule
