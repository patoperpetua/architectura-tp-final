`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 09/14/2018 03:25:45 PM
// Design Name: 
// Module Name: Execution
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Execution #(
    parameter reg_length=32,
    parameter bytes_number = $clog2(reg_length)
    )(
    input                                                   clk,
        
    input                                                   reset,
    input                                                   enable,
    
    input                        [reg_length-1:0]           reg_rt,
    input                        [reg_length-1:0]           reg_rs,
    
    input                        [reg_length-1:0]           sign,
    
    input                        [bytes_number-1:0]         shamt,
    
    input                        [3:0]                      alu_selector,
    
    input                        [1:0]                      mux_alu_1,
    input                        [1:0]                      mux_alu_2,
    
    input                        [bytes_number-1:0]         reg_dst_in,
    
    input                                                   mem_rd_in,
    input                                                   mem_wr_in,
    input                                                   reg_write_in,
    input                                                   reg_dst_selector_in,
    input                                                   halt_inst_in,
    
    //ForwardedInputs
        //From before EX
    input                        [reg_length-1:0]           alu_in,
        //From MEM
    input                        [reg_length-1:0]           reg_data,
    
    output  reg                  [reg_length-1:0]           alu_out,
    output  reg                                             alu_zero,
    
    output  reg                  [bytes_number-1:0]         reg_dst_w_out,
    
    output  reg                                             mem_rd_out,
    output  reg                                             mem_wr_out,
    output  reg                                             reg_write_out,
    output  reg                                             halt_inst_out,
    output  reg                  [reg_length-1:0]           reg_rt_out,
    output  reg                                             reg_dst_selector_out
    );
    
    wire                        [reg_length-1:0]            alu_mux1;
    wire                        [reg_length-1:0]            alu_mux2;
        
    wire                        [reg_length-1:0]            w_alu_out;
    wire                                                    w_alu_zero;    
    
    wire                        [reg_length-1:0]            w_address_branch;
    
    mux2 #(
        .reg_length(reg_length)
    ) mux_1 (
        .in1(reg_rt),
        .in2(sign),
        .in3(alu_in),
        .in4(reg_data),
        .selector(mux_alu_1),
        .out(alu_mux1)
    );
    
    mux2 #(
        .reg_length(reg_length)
    ) mux_2 (
        .in1({{27{1'b 0}}, shamt}),
        .in2(reg_rs),
        .in3(alu_in),
        .in4(reg_data),
        .selector(mux_alu_2),
        .out(alu_mux2)
    );
    
    alu #(
            .reg_length(reg_length)
        ) alu_1(
            .a(alu_mux1),
            .b(alu_mux2),
            .op(alu_selector),
            .result(w_alu_out),
            .zero(w_alu_zero)
        );
        
    always @(posedge clk)
    begin
        //halt_inst_out <= halt_inst_in;
        if(reset)
        begin
            alu_out <= 0;
            alu_zero <= 0;
            reg_dst_w_out <= 0;
            mem_rd_out <= 0;
            mem_wr_out <= 0;
            reg_write_out <= 0;
            reg_rt_out <= 0;
            reg_dst_selector_out <= 0;
            halt_inst_out <= 0;
        end
        else if(enable)// && !halt_inst_in)
        begin
            halt_inst_out <= halt_inst_in;
            alu_out <= w_alu_out;
            alu_zero <= w_alu_zero;
            mem_rd_out <= mem_rd_in;
            mem_wr_out <= mem_wr_in;
            reg_dst_w_out <= reg_dst_in;
            reg_write_out <= reg_write_in;
            reg_rt_out <= reg_rt;
            reg_dst_selector_out <= reg_dst_selector_in;
        end
    end
endmodule
