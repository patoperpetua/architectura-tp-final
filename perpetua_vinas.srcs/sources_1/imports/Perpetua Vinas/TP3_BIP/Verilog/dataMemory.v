`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    23:15:21 11/28/2016 
// Design Name: 
// Module Name:    dataMemory 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module memory #(
    parameter size=64,
    parameter reg_length=32,
    parameter mem_lenght=$clog2(size)
    )(
	input                                                  clk,
	input                                                  enable,
	input                                                  reset,
	
	input                                                  rd,
	input                                                  wr,
	
	input          [mem_lenght-1:0]                        addr,
	input          [reg_length-1:0]                        in,
	
	//To debug
	input                                                  debug,
	input          [mem_lenght-1:0]                        debug_address,
	input          [reg_length-1:0]                        debug_data_in,
	output reg     [reg_length-1:0]                        debug_data_out,
	
	output reg     [reg_length-1:0]                        out
    );
    
	reg [reg_length-1:0] dataMemory[0:size-1];

	always @(posedge clk)
	begin
        if(reset)
        begin
            out <= 32'b11111111111111111111111111111111;
        end
        if(enable)
	    begin
	        if(wr)
            begin
                dataMemory[addr] <= in;
            end
            if(rd)
            begin
                out <= dataMemory[addr];
            end
        end
        if(debug)
        begin
            if(wr)
            begin
                dataMemory[debug_address] <= debug_data_in;
            end
                debug_data_out <= dataMemory[debug_address];
        end
	end
endmodule
