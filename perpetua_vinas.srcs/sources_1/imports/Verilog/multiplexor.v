`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    17:15:18 11/26/2016 
// Design Name: 
// Module Name:    multiplexor 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module mux #(
    parameter reg_length=32
    )(
	input selector,
	input [reg_length-1:0] in1,
	input [reg_length-1:0] in2,
	output reg [reg_length-1:0] out
    );
	always @(*)
	begin
		case(selector)
		2'b00: out = in1;
		2'b01: out = in2;
		endcase
	end

endmodule

module mux2 #(
    parameter reg_length=32
    )(
	input [1:0] selector,
	input [reg_length-1:0] in1,
	input [reg_length-1:0] in2,
	input [reg_length-1:0] in3,
	input [reg_length-1:0] in4,
	output reg [reg_length-1:0] out
    );
	always @(*)
	begin
		case(selector)
		2'b00: out = in1;
		2'b01: out = in2;
		2'b10: out = in3;
		2'b11: out = in4;
		endcase
	end

endmodule