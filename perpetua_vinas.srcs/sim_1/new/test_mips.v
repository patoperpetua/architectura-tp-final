`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12/05/2018 06:49:09 PM
// Design Name: 
// Module Name: test_mips
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module test_mips #(
    parameter reg_length=32,
    parameter reg_quantity=32,
    parameter bytes_number = $clog2(reg_length),
    parameter size=64,
    parameter mem_lenght=$clog2(size)
    )();
    
    reg                                                 clk;
    reg                                                 reset;
    reg                                                 enable;
    
    reg        [7:0]                                    tx_data;
    reg                                                 tx_start;
    
    wire                                                tx_done_tick;
    wire                                                rx_done_tick;
    
    wire                                                tx;
    wire                                                rx;
    
    UART #() uart(
        .reset(reset),
        .clk(clk),
        
        .tx_in(tx_data),
        .tx_start(tx_start),
        
        .tx(tx),
        .rx(rx),
        
        .tx_done_tick(tx_done_tick),
        .rx_done_tick(rx_done_tick)
    );
      
    MIPS #(
        .reg_length(reg_length),
        .reg_quantity(reg_quantity),
        .bytes_number(bytes_number),
        .size(size),
        .mem_lenght(mem_lenght)
    ) mips(
        .clk(clk),
        .reset_hw(reset),
        .enable_hw(enable),
        .rx(tx),
        .tx(rx),
        
        .state(),
        .sub_state()
    );
    
    initial
    begin
        clk <= 0;
        reset <= 0;
        enable <= 0;
        tx_data <= 0;
        tx_start <= 0;
        #10
        reset <= 1;
        #10
        reset <= 0;
        enable <= 1;
        #10
        tx_data <= 8'b00000001;
        tx_start <= 1;
        //Clear register 0
        // ANDI 0 1 0 
        //      001100 00001 00000  00000000 00000000
        //ANDI: 00110000 00100000 00000000 00000000
        // 30 20 00 00
        //PC = 0
        #600000
        tx_data <= 8'b00110000;
        tx_start <= 1;
        #300000
        tx_data <= 8'b00100000;
        tx_start <= 1;
        #300000
        tx_data <= 8'b00000000;
        tx_start <= 1;
        #300000
        tx_data <= 8'b00000000;
        tx_start <= 1;
        //Clear R1
        //ADDU 1 0 0
        //ADDU: 000000 00000 00000 00001 00000 100001
        //ADDU: 00000000 00000000  00001000 00100001
        // 00 00 08 21
        // PC = 1
        #300000
        tx_data <= 8'b00000000;
        tx_start <= 1;
        #300000
        tx_data <= 8'b00000000;
        tx_start <= 1;
        #300000
        tx_data <= 8'b00001000;
        tx_start <= 1;
        #300000
        tx_data <= 8'b00100001;
        tx_start <= 1;
        //Clear R31
        // ADDU 31 0 0
        //ADDU: 000000 00000 00000 11111 00000 100001
        //ADDU: 00000000 00000000 11111000 00100001
        // 00 00 F8 21
        // PC = 2
        #300000
        tx_data <= 8'b00000000;
        tx_start <= 1;
        #300000
        tx_data <= 8'b00000000;
        tx_start <= 1;
        #300000
        tx_data <= 8'b11111000;
        tx_start <= 1;
        #300000
        tx_data <= 8'b00100001;
        tx_start <= 1;
        //Clear R20
        // ADDU 20 0 0
        //ADDU: 000000 00000 00000 10100 00000 100001
        //ADDU: 00000000 00000000 10100000 00100001
        // 00 00 A0 21
        // PC = 3
        #300000
        tx_data <= 8'b00000000;
        tx_start <= 1;
        #300000
        tx_data <= 8'b00000000;
        tx_start <= 1;
        #300000
        tx_data <= 8'b10100000;
        tx_start <= 1;
        #300000
        tx_data <= 8'b00100001;
        tx_start <= 1;
        //R15 = 60 = 0x3C
        //ADDI 15 0 60
        //ADDI: 001000 00000 01111 00000000 00111100
        //ADDI: 00100000 00001111  00000000 00111100
        // ADDI 20 0F 00 3C
        // PC = 4
        #300000
        tx_data <= 8'b00100000;
        tx_start <= 1;
        #300000
        tx_data <= 8'b00001111;
        tx_start <= 1;
        #300000
        tx_data <= 8'b00000000;
        tx_start <= 1;
        #300000
        tx_data <= 8'b00111100;
        tx_start <= 1;
        //Clear R14
        // ADDU 14 0 0
        //ADDU: 000000 00000 00000 01110 00000 100001
        //ADDU: 00000000 00000000 01110000 00100001
        // PC = 5
        // 00 00 70 21
        #300000
        tx_data <= 8'b00000000;
        tx_start <= 1;
        #300000
        tx_data <= 8'b00000000;
        tx_start <= 1;
        #300000
        tx_data <= 8'b01110000;
        tx_start <= 1;
        #300000
        tx_data <= 8'b00100001;
        tx_start <= 1;
        //Clear R23
        // ADDU 23 0 0
        //ADDU: 000000 00000 00000 10111 00000 100001
        //ADDU: 00000000 00000000 10111000 00100001
        // 00 00 B8 21
        // PC = 6
        #300000
        tx_data <= 8'b00000000;
        tx_start <= 1;
        #300000
        tx_data <= 8'b00000000;
        tx_start <= 1;
        #300000
        tx_data <= 8'b10111000;
        tx_start <= 1;
        #300000
        tx_data <= 8'b00100001;
        tx_start <= 1;
        //Clear M3. Store R0 in M3
        //SB 0 3(31): 101000 11111 00000 0000000000000011
        //SB:         10100011  11100000 00000000 00000011
        // A3 E0 00 03
        // PC = 7
        #300000
        tx_data <= 8'b10100011;
        tx_start <= 1;
        #300000
        tx_data <= 8'b11100000;
        tx_start <= 1;
        #300000
        tx_data <= 8'b00000000;
        tx_start <= 1;
        #300000
        tx_data <= 8'b00000011;
        tx_start <= 1;
        //Clear M12. Store R0 in M12
        //SB 0 12(31): 101000 11111 00000 0000000000001100
        //SB:          10100011  11100000 00000000 00001100
        // A3 E0 00 0C
        // PC 0 8
        #300000
        tx_data <= 8'b10100011;
        tx_start <= 1;
        #300000
        tx_data <= 8'b11100000;
        tx_start <= 1;
        #300000
        tx_data <= 8'b00000000;
        tx_start <= 1;
        #300000
        tx_data <= 8'b00001100;
        tx_start <= 1;
        //Store 60 in M22
        //SB 15 22(31): 101000 11111 01111 00000000 00010110
        //SB:           10100011  11101111 00000000 00010110
        // A3 EF 00 16
        // PC = 9
        #300000
        tx_data <= 8'b10100011;
        tx_start <= 1;
        #300000
        tx_data <= 8'b11101111;
        tx_start <= 1;
        #300000
        tx_data <= 8'b00000000;
        tx_start <= 1;
        #300000
        tx_data <= 8'b00010110;
        tx_start <= 1;
        //R11 = 60
        //ADDU 11 0 15
        //ADDU: 000000 00000 01111 01011 00000 100001
        //ADDU: 00000000  00001111 01011000  00100001
        //00 0F 58 21
        // PC = 10
        #300000
        tx_data <= 8'b00000000;
        tx_start <= 1;
        #300000
        tx_data <= 8'b00001111;
        tx_start <= 1;
        #300000
        tx_data <= 8'b01011000;
        tx_start <= 1;
        #300000
        tx_data <= 8'b00100001;
        tx_start <= 1;
        //Clear R12
        // ADDU 12 31 0
        //ADDU: 000000 11111 00000 01100 00000 100001
        //ADDU: 00000011 11100000  01100000  00100001
        //03 E0 60 21
        // PC = 11
        #300000
        tx_data <= 8'b00000011;
        tx_start <= 1;
        #300000
        tx_data <= 8'b11100000;
        tx_start <= 1;
        #300000
        tx_data <= 8'b01100000;
        tx_start <= 1;
        #300000
        tx_data <= 8'b00100001;
        tx_start <= 1;
        //ADDU 20 0 31
        //Clear R20
        //ADDU: 000000 00000 11111 10100 00000 100001
        //ADDU: 00000000  00011111 10100000  00100001
        //00 1F A0 21
        // PC = 12
        #300000
        tx_data <= 8'b00000000;
        tx_start <= 1;
        #300000
        tx_data <= 8'b00011111;
        tx_start <= 1;
        #300000
        tx_data <= 8'b10100000;
        tx_start <= 1;
        #300000
        tx_data <= 8'b00100001;
        tx_start <= 1;
        //Load 60 in R28
        //LB 28 22(31): 100000 11111 11100 00000000 00010110
        //SB:           10000011 11111100  00000000 00010110
        // 83 FC 00 16
        // PC = 13
        #300000
        tx_data <= 8'b10000011;
        tx_start <= 1;
        #300000
        tx_data <= 8'b11111100;
        tx_start <= 1;
        #300000
        tx_data <= 8'b00000000;
        tx_start <= 1;
        #300000
        tx_data <= 8'b00010110;
        tx_start <= 1;
        //Shift left with shampt
        //R19 = 120
        //SLL 19 15 1
        // Rt=15, Rd=19, shampt=2
        //000000 00000 01111 10011 00001 000000
        //00000000  00001111 10011000  01000000
        // 00 0F 98 40
        // PC = 14
        #300000
        tx_data <= 8'b00000000;
        tx_start <= 1;
        #300000
        tx_data <= 8'b00001111;
        tx_start <= 1;
        #300000
        tx_data <= 8'b10011000;
        tx_start <= 1;
        #300000
        tx_data <= 8'b01000000;
        tx_start <= 1;
        //R5 = R15 + R19 = 60 + 120 = 180 = B4 (forwarding).
        //ADDU 5 19 15 Rd=5 Rt=15 Rs=19
        //ADDU: 000000 10011 01111 00101 00000 100001
        //ADDU: 00000010 01101111  00101000  00100001
        //02 6F 28 21
        //PC = 15
        #300000
        tx_data <= 8'b00000010;
        tx_start <= 1;
        #300000
        tx_data <= 8'b01101111;
        tx_start <= 1;
        #300000
        tx_data <= 8'b00101000;
        tx_start <= 1;
        #300000
        tx_data <= 8'b00100001;
        tx_start <= 1;
        //R7 = R15 && R19 = 60 && 120 = 56 = 38(forwarding).
        //AND 7 19 15 Rd=7 Rt=15 Rs=19
        //AND: 000000 10011 01111 00111 00000 100100
        //AND: 00000010 01101111  00111000  00100100
        //02 6F 38 24
        // PC = 16
        #300000
        tx_data <= 8'b00000010;
        tx_start <= 1;
        #300000
        tx_data <= 8'b01101111;
        tx_start <= 1;
        #300000
        tx_data <= 8'b00111000;
        tx_start <= 1;
        #300000
        tx_data <= 8'b00100100;
        tx_start <= 1;
        //Load 60 in R17
        //LB 17 22(31): 100000 11111 10001 0000000000010110
        //:             10000011  11110001 00000000 00010110
        // 83 F1 00 16
        // PC = 17
        #300000
        tx_data <= 8'b10000011;
        tx_start <= 1;
        #300000
        tx_data <= 8'b11110001;
        tx_start <= 1;
        #300000
        tx_data <= 8'b00000000;
        tx_start <= 1;
        #300000
        tx_data <= 8'b00010110;
        tx_start <= 1;
        //R6 = R17 - R28 = 120 - 120 = 0 (halt 1 cycle).
        //SUBU 6 17 28 Rd=6 Rt=28 Rs=17
        //SUBU: 000000 10001 11100 00110 00000 100011
        //SUBU: 00000010 00111100  00110000  00100011
        //02 3C 30 23
        // PC = 18
        #300000
        tx_data <= 8'b00000010;
        tx_start <= 1;
        #300000
        tx_data <= 8'b00111100;
        tx_start <= 1;
        #300000
        tx_data <= 8'b00110000;
        tx_start <= 1;
        #300000
        tx_data <= 8'b00100011;
        tx_start <= 1;
        //Jump to 24
        //J 24
        //000010 000000 00000 00000 00000 011000
        //00001000  00000000 00000000  000011000
        //08 00 00 18
        // PC = 19
        #300000
        tx_data <= 8'b00001000;
        tx_start <= 1;
        #300000
        tx_data <= 8'b00000000;
        tx_start <= 1;
        #300000
        tx_data <= 8'b00000000;
        tx_start <= 1;
        #300000
        tx_data <= 8'b00011000;
        tx_start <= 1;
        //Not execute
        //R10 = 60
        //ADDU 10 15 0
        //ADDU: 000000 01111 00000 01010 00000 100001
        //ADDU: 00000001 11100000  01010000  00100001
        //01 E0 50 21
        // PC = 20
        #300000
        tx_data <= 8'b00000001;
        tx_start <= 1;
        #300000
        tx_data <= 8'b11100000;
        tx_start <= 1;
        #300000
        tx_data <= 8'b01010000;
        tx_start <= 1;
        #300000
        tx_data <= 8'b00100001;
        tx_start <= 1;
        //Not execute
        //R10 = 60
        //ADDU 10 15 0
        //ADDU: 000000 01111 00000 01010 00000 100001
        //ADDU: 00000001 11100000  01010000  00100001
        //01 E0 50 21
        // PC = 21
        #300000
        tx_data <= 8'b00000001;
        tx_start <= 1;
        #300000
        tx_data <= 8'b11100000;
        tx_start <= 1;
        #300000
        tx_data <= 8'b01010000;
        tx_start <= 1;
        #300000
        tx_data <= 8'b00100001;
        tx_start <= 1;
        //Not execute
        //R10 = 60
        //ADDU 10 15 0
        //ADDU: 000000 01111 00000 01010 00000 100001
        //ADDU: 00000001 11100000  01010000  00100001
        //01 E0 50 21
        // PC = 22
        #300000
        tx_data <= 8'b00000001;
        tx_start <= 1;
        #300000
        tx_data <= 8'b11100000;
        tx_start <= 1;
        #300000
        tx_data <= 8'b01010000;
        tx_start <= 1;
        #300000
        tx_data <= 8'b00100001;
        tx_start <= 1;
        //Not execute
        //R10 = 60
        //ADDU 10 15 0
        //ADDU: 000000 01111 00000 01010 00000 100001
        //ADDU: 00000001 11100000  01010000  00100001
        //01 E0 50 21
        // PC = 23
        #300000
        tx_data <= 8'b00000001;
        tx_start <= 1;
        #300000
        tx_data <= 8'b11100000;
        tx_start <= 1;
        #300000
        tx_data <= 8'b01010000;
        tx_start <= 1;
        #300000
        tx_data <= 8'b00100001;
        tx_start <= 1;
        //Branch to PC+3
        //BEQ 28 17 3
        //000100 11100 10001 00000000 00000011
        //00010011  10010001 00000000 00000011
        //13 91 00 03
        // PC = 24
        #300000
        tx_data <= 8'b00010011;
        tx_start <= 1;
        #300000
        tx_data <= 8'b10010001;
        tx_start <= 1;
        #300000
        tx_data <= 8'b00000000;
        tx_start <= 1;
        #300000
        tx_data <= 8'b00000011;
        tx_start <= 1;
        //Instruction must not be executed.
        //R8 = R19 + R15 = 120 + 60 = 180 = B4
        //ADDU 8 15 19
        //ADDU: 000000 01111 10011 01000 00000 100001
        //ADDU: 00000001 11110011  01000000  00100001
        //01 F3 40 21
        // PC = 25
        #300000
        tx_data <= 8'b00000001;
        tx_start <= 1;
        #300000
        tx_data <= 8'b11110011;
        tx_start <= 1;
        #300000
        tx_data <= 8'b01000000;
        tx_start <= 1;
        #300000
        tx_data <= 8'b00100001;
        tx_start <= 1;
        //Instruction must not be executed.
        //R18 = R19 + R15 = 120 + 60 = 180 = B4
        //ADDU 18 15 19
        //ADDU: 000000 01111 10011 10010 00000 100001
        //ADDU: 00000001 11110011  10010000  00100001
        //01 F3 90 21
        // PC = 26
        #300000
        tx_data <= 8'b00000001;
        tx_start <= 1;
        #300000
        tx_data <= 8'b11110011;
        tx_start <= 1;
        #300000
        tx_data <= 8'b10010000;
        tx_start <= 1;
        #300000
        tx_data <= 8'b00100001;
        tx_start <= 1;
        //Finish program
        // PC = 27
        #300000
        tx_data <= 8'b11111111;
        tx_start <= 1;
        #300000
        tx_data <= 8'b11111111;
        tx_start <= 1;
        #300000
        tx_data <= 8'b11111111;
        tx_start <= 1;
        #300000
        tx_data <= 8'b11111111;
        tx_start <= 1;
        //Continuos signal.
        #300000
        tx_data <= 8'b00000010;
        tx_start <= 1;
        //Step signal.
//        #300000
//        tx_data <= 8'b00000100;
//        tx_start <= 1;
//        //Step
//        #300000
//        tx_data <= 8'b00010000;
//        tx_start <= 1;
//        //Step
//        #94000000
//        tx_data <= 8'b00010000;
//        tx_start <= 1;
//        //Step
//        #94000000
//        tx_data <= 8'b00010000;
//        tx_start <= 1;
//        //Step
//        #94000000
//        tx_data <= 8'b00010000;
//        tx_start <= 1;
//        //Step
//        #94000000
//        tx_data <= 8'b00010000;
//        tx_start <= 1;
//        //Step
//        #94000000
//        tx_data <= 8'b00010000;
//        tx_start <= 1;
//        //Step
//        #94000000
//        tx_data <= 8'b00010000;
//        tx_start <= 1;
    end
    always
    begin
        #1 clk = ~clk;
        if(tx == 0 && tx_start == 1)
            tx_start <= 0;
    end
endmodule
