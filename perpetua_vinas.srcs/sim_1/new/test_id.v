`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/22/2018 08:48:06 PM
// Design Name: 
// Module Name: test_id
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module test_id #(
    parameter reg_length=32,
    parameter reg_quantity=32,
    parameter bytes_number = $clog2(reg_length)
)();

    reg                                                 clk;
    reg                                                 reset;
    reg                                                 enable;
    
    reg     [reg_length-1:0]                            inst;
    reg     [reg_length-1:0]                            address_pc;
    
    reg                                                 debug;
    reg     [bytes_number-1:0]                          debug_address;
    wire    [reg_length-1:0]                            debug_data;

    reg                                                 reg_write;
    reg     [bytes_number-1:0]                          reg_dst;
    reg     [reg_length-1:0]                            reg_data;
    
    wire    [reg_length-1:0]                            inst_id;
    wire    [bytes_number-1:0]                          rt;
    wire    [bytes_number-1:0]                          rs;
    wire    [reg_length-1:0]                            reg_rt;
    wire    [reg_length-1:0]                            reg_rs;
    wire    [reg_length-1:0]                            sign;
    wire    [4:0]                                       shamt;
    wire    [3:0]                                       alu_selector;
    wire    [bytes_number-1:0]                          reg_dst_id_ex;
    wire                                                mem_rd_id_ex;
    wire                                                mem_wr_id_ex;
    wire                                                reg_write_id_ex;
    wire    [reg_length-1:0]                            address_branch;
    wire    [reg_length-1:0]                            debug_data_reg;
    wire                                                w_inst_selector;
    wire                                                halt_inst_id_ex;
    
    InstructionDecoder #(
        .reg_length(reg_length),
        .reg_quantity(reg_quantity),
        .bytes_number(bytes_number)
    ) ID (
        //Signals
        .clk(clk),
        .reset(reset),
        .enable(enable),
        
        //Inputs 
            //From DebugUnit
            .debug(debug),
            .debug_address(debug_address),
            .debug_data(debug_data),
            //From IF
            .inst(inst),
            .address_pc(address_pc),
            
            //From MEM
            .reg_write(reg_write),
            .reg_dst(reg_dst),
            .reg_data(reg_data),
            
        //Outputs
            //To Forwarding Unit
            .inst_out(inst_id),
            .rt(rt),
            .rs(rs),
            
            //To IF
            .address_branch(address_branch),
            .inst_selector(w_inst_selector),
                
            //To EX
            .alu_selector(alu_selector),
            .reg_rt(reg_rt),
            .reg_rs(reg_rs),        
            .out_sign(sign),
            .out_shamt(shamt),
            .mem_rd(mem_rd_id_ex),
            .mem_wr(mem_wr_id_ex),
            .reg_write_out(reg_write_id_ex),
            .halt_inst(halt_inst_id_ex),
                
            //To MEM
            .reg_dst_out(reg_dst_id_ex)
    );
    initial
    begin
        clk = 0;
        reset = 1;
        enable = 0;
        //Debug mode.
        debug = 0;
        #5
        //Stage Enabled.
        reset = 0;
        enable = 1;
        //Test 1: Write registers.
        reg_write = 1;
        reg_dst = 1;
        reg_data = 10;
        #10
        reg_dst = 2;
        reg_data = 20;
        #10
        reg_dst = 3;
        reg_data = 30;
        //Test 2: Read registers. 
        #10
        reg_write = 0;
        //SLLV RS=1, RT=3, RD=16
        inst = 32'b00000000001000111000000000000100;
        //Test 3: read registers from debug.
        #10
        debug = 1;
        debug_address = 1;
        #10
        debug_address = 2;
        #10
        debug = 0;
        //Test 4: split interruption
        //Tipo R: inmediato. RT:16, RD:8,RS: 4
        #10
        inst = 32'b00000000000100000100000100000000;
        //Tipo R: shift. RS:16, RT:8,RD: 4
        #10
        inst = 32'b00000010000010000010000000000100;
        //Tipo R: aritmetico. RS:16, RT:8,RD: 4
        #10
        inst = 32'b00000010000010000010000000100001;
        //Tipo R: jump. RS:16, RD:8.
        #10
        inst = 32'b00000010000000000100000000001000;
        //Tipò I: load. RS:16, RT:8. Offset: 2
        #10
        inst = 32'b10000010000010000000000000000010;
        //Tipo I: store. RS:16, RT:8. Offset: 2
        #10
        inst = 32'b10100010000010000000000000000010;
        //Tipo I: inmediato. RS:16, RT:8. Inmmediate: 2 
        #10
        inst = 32'b00100010000010000000000000000010;
        //Tipo I: branch.  RS:16, RT:8. Offset: 2 
        #10
        inst = 32'b00010010000010000000000000000010;
        //Tipo J: address: 15.
        // Decimal: 134217743, hexa: 800000F
        #10
        inst = 32'b00001000000000000000000000001111;
        //Halt. Decimal: 4230184964, hexa: FC238004
        #10
        inst = 32'b11111100001000111000000000000100;
        //Test 5: address branch.
        //Decimal: 570949634, Hexa: 22080002
        #10
        reg_write = 1;
        reg_dst = 8;
        reg_data = 1000;
        #10
        reg_dst = 16;
        reg_data = 1000;
        #10
        address_pc = 4;
        reg_write = 0;
        inst = 32'b00100010000010000000000000000010;
        //Tipo I: branch.  RS:16, RT:8. Offset: 2 
    end
    always 
        #5 clk = ~clk;
endmodule
