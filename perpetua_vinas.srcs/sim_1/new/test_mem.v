`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/27/2018 03:18:27 PM
// Design Name: 
// Module Name: test_mem
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module test_mem #(
    parameter reg_length=32,
    parameter reg_quantity=32,
    parameter bytes_number = $clog2(reg_length),
    parameter mem_lenght = $clog2(size),
    parameter size = 64
)();

    reg                                                 clk;
    reg                                                 reset;
    reg                                                 enable;
    reg                                                 debug;
    reg     [mem_lenght-1:0]                            debug_address_mem;

    reg     [reg_length-1:0]                            alu_result;
    reg     [reg_length-1:0]                            reg_data_in;
    
    reg     [bytes_number-1:0]                          reg_dest_ex_mem;
    reg                                                 mem_rd;
    reg                                                 mem_wr;
    reg                                                 reg_write_ex_mem;
    reg                                                 halt_inst;
    reg                                                 reg_dst_selector;

    wire                                                reg_write;
    wire    [bytes_number-1:0]                          reg_dst;
    wire    [reg_length-1:0]                            reg_data;
    wire    [reg_length-1:0]                            debug_data_mem;
    
    MemoryAccess #(
        .reg_length(reg_length),
        .bytes_number(bytes_number),
        .size(size),
        .mem_lenght(mem_lenght)
    ) MEM (
        //Signals
        .clk(clk),
        .reset(reset),
//        .reset(reset | reset_mem),
        .enable(enable),
        .debug(debug),
        
        //Inputs
            //From EX
                .alu_result(alu_result),
                
                .reg_dst_in(reg_dest_ex_mem),
                .reg_data_in(reg_data_in),
                .reg_write_in(reg_write_ex_mem),
                
                .mem_rd(mem_rd),
                .mem_wr(mem_wr),
                .halt_inst_in(halt_inst),
                .reg_dst_selector(reg_dst_selector),
            //From Debug
                .debug_address(debug_address_mem),
                
        //Outputs
            //To DE
            .reg_write_out(reg_write),
            .reg_dst_out(reg_dst),
            .reg_data_out(reg_data),
            
            //To DebugUnit
            .debug_data(debug_data_mem)
    );
    initial
    begin
        clk = 0;
        reset = 1;
        enable = 0;
        halt_inst = 0;
        debug = 0;
        debug_address_mem = 1;
        alu_result = 15;
        reg_data_in = 10;
        mem_rd = 0;
        mem_wr = 0;
        reg_write_ex_mem = 0;
        reg_dst_selector = 0;
        reg_dest_ex_mem = 0;
    #5    
        //Stage Enabled.
        reset = 0;
        enable = 1;
        mem_wr = 1;
    #10
        mem_wr = 0;
        reg_write_ex_mem = 1;
        reg_dst_selector = 1;
        reg_dest_ex_mem = 5;
    #10
        reg_dst_selector = 0;
        mem_rd = 1;
        reg_dest_ex_mem = 2;
    #10
        mem_rd = 0;
        halt_inst = 1;
    #10
        enable = 0;
        halt_inst = 0;
        debug = 1;
        debug_address_mem = 15;
    end
    always 
        #5 clk = ~clk;
endmodule
